﻿namespace System
{
    public static class DateTimeHelpers
    {
        public static double GetLeftMargin(DateTime eventStart, DateTime calendarStartDate)
        {
            var left = 0d;

            // if event start year is in next year, then to calculate the left margin, we have to split it into two parts.
            // first part if from calendar start date to the 31st of Dec
            // second part if from 1st Jan of next year to the start day of the event.
            // adding first part (previous years months) left margin to the second part will give the accurate margin.
            if (eventStart.Year > calendarStartDate.Year)
            {
                // Calculate the difference of event start date from the 1st Jan of next year.
                var differenceOfNextYear = eventStart - new DateTime(calendarStartDate.Year + 1, 1, 1);
                left += (differenceOfNextYear.Days / 30.0 * 100.0 * 2) + (differenceOfNextYear.Days % 30.0 * (1 / 200.0));

                // Calculate the difference of calendar start date to the 31st of Dec.
                var previousYearDifference = new DateTime(calendarStartDate.Year, 12, 31) - calendarStartDate;
                left += (previousYearDifference.Days / 30.0 * 100.0 * 2) + (previousYearDifference.Days % 30.0 * (1 / 200.0));
            }
            else
            {
                var difference = eventStart - calendarStartDate;
                left += (difference.Days / 30.0 * 100.0 * 2) + (difference.Days % 30.0 * (1 / 200.0));
            }

            return left;
        }

        public static double GetEventWidth(DateTime eventStart, DateTime eventEnd)
        {
            var difference = eventEnd - eventStart;

            return (difference.Days / 30.0 * 100.0 * 2) + (difference.Days % 30.0 * (1 / 200.0));
        }
    }
}
