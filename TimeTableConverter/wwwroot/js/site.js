﻿/// <reference path="../lib/dayjs/dayjs.js" />
/// <reference path="dayjs-helpers.js" />
// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var utilities = (function () {
    function toggleOverLay() {
        var windowHeigth = document.body.scrollHeight;
        var overlayElem = document.getElementById('overlay');

        if (overlayElem.classList.contains('active')) {
            overlayElem.classList.replace('active', 'hidden');
        }
        else {
            overlayElem.style.height = windowHeigth + "px";
            overlayElem.classList.replace('hidden', 'active');
        }
    }

    return {
        toggleOverLay: toggleOverLay
    };
})();

(function ($, dayjs, dateHelpers, utilities, initialTimetableData) {
    "use strict";
    $(function () {
        (function () {
            utilities.toggleOverLay();

            const apiDateFormat = 'YYYY-MM-DD';
            const dataDateFormat = 'YYYY-MM-DD';
            const timeIndicatorDateFormat = 'MMM D/YY';

            let showProgram = $(".tl_timeline").attr("data-showProgram");

            let eventId = 0, eventId1 = 0, i = 0, j = 0, j1 = 0;

            $(".tl_timeline").each(function () {
                let $self = $(this);
                let timeTablesData = initialTimetableData;

                setupInitialData();
                if (!$self.hasClass('auto-load')) generateTimetable();

                $self.find('.tl_next').on('click', function (event) {
                    event.preventDefault();
                    if (!isReachedAtRight) return;

                    if (timeTablesData.calendarEndDate.isBefore(timeTablesData.dataAvailUntilDate)) {
                        utilities.toggleOverLay();

                        timeTablesData.calendarEndDate = timeTablesData.calendarEndDate.add(1, 'year');

                        handleAjaxRequest(
                            getEvents(timeTablesData.calendarEndDate.subtract(1, 'year').format(apiDateFormat), timeTablesData.calendarEndDate.format(apiDateFormat)).then(
                                function (successResponse) { addTimetableToExisting(successResponse); generateTimetable(); },
                                function (errorResponse) { console.log(errorResponse); }
                            ));
                    }

                    return false;
                });

                $self.find('.tl_previous').on('click', function (event) {
                    event.preventDefault();
                    if (!isReachedAtLeft) return;

                    if (timeTablesData.calendarStartDate.isAfter(timeTablesData.dataAvailFromDate)) {
                        utilities.toggleOverLay();

                        timeTablesData.calendarStartDate = timeTablesData.calendarStartDate.subtract(1, 'year');

                        handleAjaxRequest(
                            getEvents(timeTablesData.calendarStartDate.format(apiDateFormat), timeTablesData.calendarStartDate.add(1, 'year').format(apiDateFormat)).then(
                                function (successResponse) { addTimetableToExisting(successResponse); generateTimetable(); },
                                function (errorResponse) { console.log(errorResponse); }
                            ));
                    }

                    return false;
                });

                function generateTimetable() {
                    insertHeader(timeTablesData.fiscalYearTitle, timeTablesData.fiscalYearStartDate, timeTablesData.fiscalYearEndDate);

                    let tlSlideableHtml = "";
                    let locations = timeTablesData['location'];
                    eventId = 0, eventId1 = 0, j = 0, j1 = 0;

                    for (i = 1; i <= locations.length; i++) {
                        let currentLocation = locations[i - 1];
                        let locationEvents = currentLocation['taxEvents'];

                        tlSlideableHtml += `<div class='tl_location'>
<div class='tl_the_location' data-location='${i}'>
                    <h3>${currentLocation.name}</h3>
                    <p class='tl_the_location_subtitle'>${currentLocation.subtext}</p>
                </div>
<div class='tl_the_timeline'>
<div class='tl_the_timeline_content tl_slidable'>
`;

                        tlSlideableHtml += eventsToHTML(locationEvents);

                        tlSlideableHtml += `
<!-- closing tl_the_timeline_content tl_slidable div -->
</div>
`;
                        tlSlideableHtml += generateEventDetailsHtml(locationEvents);

                        tlSlideableHtml += `
<!-- closing tl_the_timeline div -->
</div>
<div class='tl_clear'>&nbsp;</div>
 <!-- closing tl_location div -->
</div>
`;
                    }

                    $self.find('.tl_location').remove();
                    $self.append(tlSlideableHtml);

                    timeTable($self);
                }

                function insertHeader(fiscalYearTitle, fiscalYearStartDate, fiscalYearEndDate) {
                    let timeSelectorBarHtml = generateTimeSelectorBar(fiscalYearTitle, fiscalYearStartDate, fiscalYearEndDate);
                    $self.find('div.tl_time_selector_bar').replaceWith(timeSelectorBarHtml);

                    let timeIndicatorHtml = generateTimeIndicatorHtml();
                    $self.find('div.tl_time_indicator').replaceWith(timeIndicatorHtml);

                }

                function generateTimeSelectorBar() {
                    let html = `
        <div style="left: 0px;" class="tl_time_selector_bar">
            <div class="tl_slidable">
                <ul>`;

                    var calendarStartDate = timeTablesData.calendarStartDate.subtract(1, 'day');

                    Object.keys(timeTablesData.fiscalYearsDetails).forEach(function (key, index) {
                        calendarStartDate = calendarStartDate.add(1, 'day');

                        var fiscalYearDetails = timeTablesData.fiscalYearsDetails[calendarStartDate.format(dataDateFormat)];
                        var differenceInMonths = fiscalYearDetails.fiscalYearEndDate.diff(fiscalYearDetails.fiscalYearStartDate, 'month') + 1;
                        var fiscalYearWidth = differenceInMonths * 2 * 100;

                        html += `<li style="text-align: end; border-left: 0px; background: ${index % 2 === 0 ? "rgba(0,0,0,.2)" :"#ebebeb;"}; padding:0 0 0 0px!important; width: ${fiscalYearWidth}px;"><span style="padding: 0 10px 0 0!important;">Fiscal Year ending ${fiscalYearDetails.fiscalYearTitle}</span></li>`;

                        calendarStartDate = fiscalYearDetails.fiscalYearEndDate;
                    });

                    const count = timeTablesData.calendarEndDate.diff(timeTablesData.calendarStartDate, 'month') * 2.0;
                    $self.attr('data-hours', count - 3);

                    html += `
                </ul>
            </div>
            <div class="tl_clear">&nbsp;</div>
        </div>`;
                    return html;
                }

                function generateTimeIndicatorHtml() {
                    let html = `
            <!-- Time Indicator -->
            <div class="tl_time_indicator">
                <div class="tl_slidable">
<ul>
`;
                    let count = timeTablesData.calendarEndDate.diff(timeTablesData.calendarStartDate, 'month') * 2.0;
                    let date = timeTablesData.calendarStartDate.clone();

                    for (let x = 0; x <= count; x++) {
                        html += `<li style="${x === 0 ? 'border-left: 0px;' : ''}">${date.format(timeIndicatorDateFormat)}</li>`;

                        // if day is 15, then move to the next month's first day.
                        if (date.date() === 15) { date = date.add(1, 'month').subtract(14, 'days'); }

                        // else add 14 days to get the 15th day of the current moth.
                        else { date = date.add(14, 'days'); }
                    }

                    html += `

                    </ul>
                    <div class="tl_clear">&nbsp;</div>
                </div>
            </div>`;

                    return html;
                }

                function eventsToHTML(events) {
                    let html = ``;

                    for (let index = 0; index < events.length; index++) {
                        j++;
                        eventId++;

                        let currentEvent = events[index];
                        let eventStartDate = dayjs(currentEvent.calendarStart);
                        let eventEndDate = dayjs(currentEvent.calendarEnd);
                        let isOneDayEvent = eventEndDate.diff(eventStartDate, 'day') === 0;

                        if (isOneDayEvent) {
                            html += generateoneDayEventHtml(currentEvent);
                        }
                        else {
                            html += generateMultiDayEventHtml(currentEvent);
                        }
                    }

                    return html;
                }

                function generateEventDetailsHtml(events) {
                    let addProgramText = $self.attr("data-addProgramText");
                    let removeProgramText = $self.attr("data-removeProgramText");

                    let html = "";
                    for (let index = 0; index < events.length; index++) {
                        j1++;
                        eventId++;
                        let currentEvent = events[index];
                        let eventStartDate = dayjs(currentEvent.calendarStart);
                        html += `
<div class='tl_event_details' data-start='${eventStartDate.format(dataDateFormat)}' data-event='${j1}'>`;

                        if (currentEvent.image) {
                            html += `<a href='${currentEvent.image}' class='tl_event_image' title='${currentEvent.title}'><img alt='${currentEvent.title}' src='${currentEvent.image}'></a>
`;
                        }

                        if (currentEvent.youtube) {
                            html += `<a data-group='youtube' class='youtube' href='http://www.youtube.com/embed/${currentEvent.youtube}?wmode=transparent&amp;showsearch=0&amp;version=3&amp;iv_load_policy=3&amp;showinfo=1&amp;rel=0&amp;autoplay=1&amp;theme=light&amp;color=red&amp;modestbranding=1'><img src='http://img.youtube.com/vi/${currentEvent.youtube}/2.jpg' class='youtubethumb' alt='' width='130' height='97'></a>
`;
                        }

                        html += `
${currentEvent.description}
<div class='tl_description_bottom'>`;

                        if (currentEvent.link) {
                            html += `
<a class='tl_link' href='${currentEvent.link}' title='${currentEvent.title}' target='_blank'>${currentEvent.title}<span class='tl_link_icon'></span></a>`;
                        }

                        if (showProgram) {
                            html += `
<a href='${currentEvent.link}' data-eventid='${eventId1}' data-add-text='${addProgramText}' data-prefix='3fdfc81582234987a9dd3a1e84b541c0' data-remove-text='${removeProgramText}' data-event='${j1}' data-start='${eventStartDate.format(dataDateFormat)}' class='tl_program_button' data-val='false'>Add to program</a>`;
                        }

                        html += `
<!-- closing tl_description_bottom div -->
</div>
<div class='tl_clear'>&nbsp;</div>
<!-- closing tl_event_details div -->
</div>`;
                    }

                    return html;
                }

                function generateoneDayEventHtml(event) {
                    let margin = calculateLeftAbsoluteMargin(event);

                    let html = `<div data-show-description='${event.description ? true : false}'
                                     class="tl_event"
                                     title="${event.title}"
                                     data-eventid="${eventId}"
                                     data-event="${j}"
                                     style="left: ${margin}px; width:0px; border: 1px solid green; cursor: pointer;"
                                     data-location="${i}"
                                     data-info-showing="false">
                                    <span class="tl_event_time">${dayjs(event.calendarStart).format("MMM DD")}</span>
                                    <h2>${event.subtitle ? event.subtitle : ''}</h2>
                                    <div class="tl_event_program_indicator"></div>
                                </div>
`;

                    return html;
                }

                function generateMultiDayEventHtml(event) {
                    let html = "";
                    let margin = calculateLeftAbsoluteMargin(event);
                    let width = getEventWidth(event);

                    html = `<div data-show-description='${event.description ? true : false}'
                                     class="tl_event"
                                     data-eventid="${eventId}"
                                     data-event="${j}"
                                     style="left: ${margin}px; width:${margin === 0 ? width - 1 : width}px;"
                                     data-location="${i}">
                                    <span class="tl_event_time">${dayjs(event.calendarStart).format("MM/DD/YYYY")} - ${dayjs(event.calendarEnd).format("MM/DD/YYYY")}</span>
                                    <h2>${event.title}</h2>
                                    <p class="tl_subtitle">${event.subtitle}</p>
                                    <div class="tl_event_program_indicator"></div>
                                </div>
`;

                    return html;
                }

                function calculateLeftAbsoluteMargin(event) {
                    const diffIn15Days = dateHelpers.calculateDifferenceIn15DayDifference(timeTablesData.calendarStartDate, dayjs(event.calendarStart));


                    var leftMargin = Math.ceil(diffIn15Days.leftBoundaryDifference / (15 / 100)) + (diffIn15Days.months * 100);

                    return leftMargin;
                }

                function getEventWidth(event) {
                    const diffIn15Days = dateHelpers.calculateDifferenceIn15DayDifference(dayjs(event.calendarStart), dayjs(event.calendarEnd));


                    const difference = Math.ceil(diffIn15Days.leftBoundaryDifference / (15 / 100)) + (diffIn15Days.months * 100) + Math.ceil(diffIn15Days.rightBoundaryDifference / (15 / 100));

                    return difference;
                }

                function addTimetableToExisting(timetables) {
                    for (let i = 0; i < timetables.location.length; i++) {
                        let currentLocation = timetables.location[i];
                        let existingLocation = timeTablesData.location.find(f => f.name === currentLocation.name);
                        if (!existingLocation) {
                            timeTablesData.location.push(currentLocation);
                        }
                        else {
                            for (let j = 0; j < currentLocation.taxEvents.length; j++) {
                                existingLocation.taxEvents.push(currentLocation.taxEvents[j]);
                            }
                        }
                    }


                    timeTablesData['dataAvailFromDate'] = dayjs(timetables['dataAvailFromDate']);
                    timeTablesData['dataAvailUntilDate'] = dayjs(timetables['dataAvailUntilDate']);

                    var fiscalYearStartDate = dayjs(timetables['fiscalYearStartDate']);
                    timeTablesData.fiscalYearsDetails[fiscalYearStartDate.format(dataDateFormat)] = {
                        fiscalYearStartDate: fiscalYearStartDate,
                        fiscalYearEndDate: dayjs(timetables['fiscalYearEndDate']),
                        fiscalYearTitle: timetables['fiscalYearTitle']
                    };
                }

                function setupInitialData() {
                    timeTablesData = {
                        location: initialTimetableData.location
                    };

                    timeTablesData['calendarStartDate'] = dayjs(initialTimetableData['calendarStartDate']);
                    timeTablesData['calendarEndDate'] = dayjs(initialTimetableData['calendarEndDate']);
                    timeTablesData['dataAvailFromDate'] = dayjs(initialTimetableData['dataAvailFromDate']);
                    timeTablesData['dataAvailUntilDate'] = dayjs(initialTimetableData['dataAvailUntilDate']);

                    var fiscalYearStartDate = dayjs(initialTimetableData['fiscalYearStartDate']);

                    timeTablesData.fiscalYearsDetails = {
                        [fiscalYearStartDate.format(dataDateFormat)]: {
                            fiscalYearStartDate: fiscalYearStartDate,
                            fiscalYearEndDate: dayjs(initialTimetableData['fiscalYearEndDate']),
                            fiscalYearTitle: initialTimetableData['fiscalYearTitle']
                        }
                    };

                    // clear all initial data from memory.
                    initialTimetableData = undefined;
                    window.initialTimetableData = undefined;
                }

                function getEvents(startDate, endDate) {
                    let data = {
                        url: `https://www.taxdrcr.com/mocksvc/timetable?corrId=77733733-caac-8851-bbeb-0223af9d711b&tpyrId=96741733-c59c-e811-bce7-0004ff9d73eb&startDate=${startDate}&endDate=${endDate}`,
                        method: 'GET'
                    };

                    return $.ajax(data);
                }
            });

            function handleAjaxRequest(ajaxRequest) {
                return ajaxRequest.always(function () { utilities.toggleOverLay(); });
            }

            utilities.toggleOverLay();
        })();
    });
})(jQuery, dayjs, dateHelpers, utilities, initialTimetableData);