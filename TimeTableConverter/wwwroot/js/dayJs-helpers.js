﻿/// <reference path="../lib/dayjs/index.d.ts" />
const dateHelpers = (function () {

    function calculateDifferenceIn15DayDifference(start, end) {
        var startDate = start.clone();

        let differenceInMonths = 0;
        let leftBoundaryDifference = 0;
        let rightBoundaryDifference = 0;

        if (startDate.date() > 1 && startDate.date() < 16) {
            leftBoundaryDifference = 16 - startDate.date();

            startDate = startDate.set('date', 15);
        }
        else if (start.date() > 16) {
            leftBoundaryDifference = startDate.date() - 16;

            startDate = startDate.add(1, 'month').set('date', 1);
        }
        while (end.diff(startDate, 'day') >= 15) {
            differenceInMonths++;

            // if day is 15, then move to the next month's first day.
            if (startDate.date() >= 15) {
                startDate = startDate.add(1, 'month').set('date', 1);
            } else {
                // else add 15 days to get the 15th day of the current moth.
                startDate = startDate.set('date', 15);
            }
        }

        rightBoundaryDifference = end.diff(startDate, 'day');

        return {
            months: differenceInMonths,
            leftBoundaryDifference: leftBoundaryDifference,
            rightBoundaryDifference: rightBoundaryDifference
        };
    }


    return {
        calculateDifferenceIn15DayDifference: calculateDifferenceIn15DayDifference
    };
})();