﻿using System;
using System.Collections.Generic;

namespace TimeTableConverter.Models
{
    public class Event
    {
        public DateTime calendarStart { get; set; }
        public DateTime calendarEnd { get; set; }
        public int fiscalYearDayStart { get; set; }
        public int fiscalYearDayEnd { get; set; }
        public string title { get; set; }
        public string subtitle { get; set; }
        public string youtube { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public string image { get; set; }
    }

    public class Location
    {
        public string name { get; set; }
        public string subtext { get; set; }
        public IList<Event> taxEvents { get; set; }
    }

    public class Timetable
    {
        public DateTime calendarStartDate { get; set; }
        public DateTime calendarEndDate { get; set; }

        public DateTime fiscalYearStartDate { get; set; }
        public DateTime fiscalYearEndDate { get; set; }
        public string fiscalYearTitle { get; set; }

        public string title { get; set; }

        public DateTime dataAvailFromDate { get; set; }
        public DateTime dataAvailUntilDate { get; set; }

        public IList<Location> location { get; set; }
    }

    public class TimeTableModel
    {
        public Timetable timetable { get; set; }

        public TimeTableSettings Settings { get; set; } = new TimeTableSettings();
    }

    public class TimeTableSettings
    {
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int interval { get; set; }
        public int number_times { get; set; }
        public bool indicatorbar { get; set; }
        public string title { get; set; }
        public string prefix { get; set; }
        public string width { get; set; }
        public string print { get; set; }
        public bool quickprint { get; set; }
        public bool program { get; set; }
        public string add_program_text { get; set; }
        public string remove_program_text { get; set; }
        public string autoscroll { get; set; }
        public string print_url { get; set; }
        public bool am_pm { get; set; }

        public int year { get; set; }
        public string dateFormat { get; set; }

        public IDictionary<string, object> OtherSettings { get; set; } = new Dictionary<string, object>();
    }
}
