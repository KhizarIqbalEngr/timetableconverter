﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace TimeTableConverter.Components
{
    public class Update : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}
