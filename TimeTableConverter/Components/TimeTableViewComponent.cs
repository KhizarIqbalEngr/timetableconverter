﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using TimeTableConverter.Models;

namespace TimeTableConverter.Components
{
    public class TimeTableViewComponent : ViewComponent
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _env;

        protected TimeTableSettings _settings;
        protected const string timeTableSettingKey = "TimeTableSettings";

        public TimeTableViewComponent(IConfiguration configuration, IHostingEnvironment env)
        {
            this._configuration = configuration;
            this._env = env;
        }

        public async Task<IViewComponentResult> InvokeAsync(string url, string title, string width, string autoScroll = "em", string add ="", string remove = "", string timePrint = "em")
        {
            //Load xml file
            this._settings = new TimeTableSettings();
            this._configuration.Bind(timeTableSettingKey, this._settings);

            var webRoot = _env.WebRootPath;
            var filePath = Path.Combine(webRoot, $"../SampleData/{url}");

            if (!File.Exists(filePath))
            {
                throw new Exception($"Error: there is an error in your XML file: <a href='{filePath}'>{filePath}</a>");
            }

            if (url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("ftp://"))
            {
                throw new Exception("Error: the path to your json file may not start with \"http://\"");
            }

            //Set the settings
            var fileContents = await File.ReadAllTextAsync(filePath);
            var json = JsonConvert.DeserializeObject<TimeTableModel>(fileContents, new JsonSerializerSettings
            {
                DateFormatString = this._settings.dateFormat
            });

            this._settings.year = int.Parse(Path.GetFileNameWithoutExtension(filePath));
            this.SetTimetablePrint(timePrint);
            this.SetTimetableTitle(title);
            this.SetTimetableAutoScroll(autoScroll);
            this.SetTimetableWidth(width);
            this.SetTimetableProgram(add, remove);

            this.SetSettings(json, url);

            json.Settings = this._settings;

            return View(json);
        }

        /**
         * Sets the XML settings
         * @param string json json
         * @param string $url json url
         * @return null
         */
        private void SetSettings(TimeTableModel json, string url)
        {
            //Update settings from json
            this.UpdateJSONSettings(json);

            //Set json url
            this._settings.OtherSettings["url"] = url;

            //Set number_times
            // this._settings.number_times = DateTimeHelpers.RangeTime(this._settings.start, this._settings.end) / 100;

            //Set timetable prefix
            this._settings.prefix = url.ToMD5Hash();
        }

        /**
     * Sets the timetable title
     * @param string $title Value
     * @return null
     */
        private void SetTimetableTitle(string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                this._settings.title = title;
            }
        }

        /**
     * Sets the timetable autoscroll
     * @param mixed $val Value
     * @return null
     */
        private void SetTimetableAutoScroll(string val = "em")
        {
            if (val == "em")
            {
                this._settings.autoscroll = "true";
            }
            else
            {
                this._settings.autoscroll = val;
            }
        }

        /**
         * Sets the timetable width
         * @param int $width Value
         * @return null
         */
        private void SetTimetableWidth(string width)
        {
            //Set width
            if (!string.IsNullOrEmpty(width))
            {
                //If percentage or px
                if (width.EndsWith('%') || width.EndsWith('x'))
                {
                    this._settings.width = width;
                }
                else //Else, an integer
                {
                    this._settings.width = $"{width}px";
                }
            }
        }

        /**
         * Sets the timetable print
     * @param mixed $quick Value
     * @return null
     */
        private void SetTimetablePrint(string quick = "em")
        {
            if (quick == "em")
            {
                this._settings.print = quick;
            }
            else
            {
                this._settings.print = "true";
            }
        }

        /**
     * Sets the timetable program text
     * @param mixed $add    Add text
     * @param mixed $remove Remove text
     * @return null
     */
        private void SetTimetableProgram(string add ,string remove)
        {
            this._settings.program = true;

            if (!string.IsNullOrEmpty(add))
            {
                this._settings.add_program_text = add;
            }
            if (!string.IsNullOrEmpty(remove))
            {
                this._settings.remove_program_text= remove;
            }
        }

        /**
         * <summary>
         * Saves the XML settings in the settings of the script
         * </summary>
         * <param name="json" json with settings
         * return null
         */
        private void UpdateJSONSettings(TimeTableModel json)
        {
            if (!string.IsNullOrEmpty(json.timetable.title) && string.IsNullOrEmpty(this._settings.title))
            {
                this._settings.title = json.timetable.title;
            }
        }
    }
}
